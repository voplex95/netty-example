package netty.commons;

public class ClientConfiguration {

    public static final int CHILD_COUNT = 1;
    public static final int AWAIT_TIMEOUT = 5_000; // in milliseconds

    private ClientConfiguration(){}

}
