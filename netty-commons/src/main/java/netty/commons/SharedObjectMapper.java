package netty.commons;

import com.fasterxml.jackson.databind.ObjectMapper;

public class SharedObjectMapper {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    private SharedObjectMapper() {}

    public static ObjectMapper getInstance() {
        return objectMapper;
    }

}
