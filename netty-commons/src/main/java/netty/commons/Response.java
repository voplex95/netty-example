package netty.commons;

public class Response {

    private int status;
    private String message;

    public Response() {

    }

    public Response(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public static Response ok() {
        return new Response(200, "OK");
    }

    public static Response badRequest() {
        return new Response(400, "Bad request");
    }

    public static Response internalServerError() {
        return new Response(500, "Internal server error");
    }

    @Override
    public String toString() {
        return "Response{" + "status=" + status + ", message='" + message + '\'' + '}';
    }
}
