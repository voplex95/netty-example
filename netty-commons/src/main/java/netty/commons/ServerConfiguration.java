package netty.commons;

public class ServerConfiguration {

    public static final String HOST = "localhost";
    public static final int PORT = 8090;
    public static final int PARENT_COUNT = 1;
    public static final int CHILD_COUNT = 1;
    public static final int SO_BACKLOG = 100;
    public static final boolean SO_KEEPALIVE = true;

    private ServerConfiguration(){}

}
