package netty.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.concurrent.Promise;
import netty.commons.Response;
import netty.commons.SharedObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.Charset;
import java.util.Queue;

@Sharable
public class SocketChannelInboundHandler extends ChannelInboundHandlerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(SocketChannelInboundHandler.class);

    private static final Queue<Promise<Response>> queue = PromiseQueueSingleton.getInstance();
    private static final ObjectMapper objectMapper = SharedObjectMapper.getInstance();

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        try {
            final String json = ((ByteBuf) msg).toString(Charset.forName("utf8"));
            logger.debug("Received response -> content[\n{}\n], remote address[{}]", json, ctx.channel().remoteAddress());
            logger.debug("Attempting to deserialize JSON message");

            final Response response = objectMapper.readValue(json, Response.class);
            logger.debug("{}", response);

            logger.debug("Fulfilling promise from the queue");
            queue.remove().setSuccess(response);

            logger.debug("Queue size: {}", queue.size());
        } catch(Exception e) {
            logger.error(e.getMessage(), e);
            queue.remove().setFailure(e);
        } finally {
            ((ByteBuf) msg).release();
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        logger.debug("Caught exception: {}", cause.getMessage(), cause);
    }

}
