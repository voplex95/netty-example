package netty.client;

import io.netty.util.concurrent.Promise;
import netty.commons.Response;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class PromiseQueueSingleton {

    private static final Queue<Promise<Response>> queue = new ConcurrentLinkedQueue<>();

    public static Queue<Promise<Response>> getInstance() {
        return queue;
    }

    private PromiseQueueSingleton(){}

}
