package netty.client;

import io.netty.util.concurrent.Promise;
import netty.commons.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class ClientRunner {

    private static final Logger logger = LoggerFactory.getLogger(ClientRunner.class);

    private static final int ITERATIONS = 10_000;

    public static void main(String[] args) {
        final NettyClient client = new NettyClient();
        try {
            client.connect(ServerConfiguration.HOST, ServerConfiguration.PORT);

            final List<Promise> promiseList = new ArrayList<>();

            // Send async requests
            for(int i = 0; i < ITERATIONS; i++) {
                final Request request = request(i);
                final Promise<Response> promise = client.exchange(request);
                promiseList.add(promise);
            }

            // Wait for response
            for(int i = 0; i < promiseList.size(); i++) {
                try {
                    logger.debug("Waiting for promise #{}", i);
                    final Object result = promiseList.get(i).await(ClientConfiguration.AWAIT_TIMEOUT);
                    logger.debug("Promise is fulfilled [success={}]", promiseList.get(i).isSuccess());
                    logger.info("Received response object: {}", result);
                } catch (InterruptedException e) {
                    logger.error("Timeout. Server did not response within {} ms.", ClientConfiguration.AWAIT_TIMEOUT);
                    throw new RuntimeException(e);
                }
            }

        } finally {
            client.disconnect();
        }
    }

    private static Request request(int id) {
        final Request request = new Request();
        request.setId(id);
        request.setMessage("Hello Netty! This is message #" + id);
        return request;
    }

}
