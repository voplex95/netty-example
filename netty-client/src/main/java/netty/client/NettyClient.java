package netty.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.util.concurrent.Promise;
import netty.commons.ClientConfiguration;
import netty.commons.Request;
import netty.commons.Response;
import netty.commons.SharedObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Queue;

import static java.util.Objects.nonNull;

public class NettyClient {

    private static final Logger logger = LoggerFactory.getLogger(NettyClient.class);
    private static final ObjectMapper objectMapper = SharedObjectMapper.getInstance();

    private final Queue<Promise<Response>> queue = PromiseQueueSingleton.getInstance();
    private Channel channel;

    public void connect(String inetHost, int inetPort) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap
                .group(childGroup())
                .channel(NioSocketChannel.class)
                .option(ChannelOption.SO_KEEPALIVE, true)
                .handler(new SocketChannelInitializer());
        channel = bootstrap.connect(inetHost, inetPort).syncUninterruptibly().channel();
    }

    public void disconnect() {
        logger.info("Disconnecting from server");
        if(nonNull(channel)) {
            channel.disconnect().awaitUninterruptibly(ClientConfiguration.AWAIT_TIMEOUT);
            channel.close().awaitUninterruptibly(ClientConfiguration.AWAIT_TIMEOUT);
            logger.info("Successfully disconnected");
        } else {
            logger.warn("No active channel was found");
        }
    }

    private EventLoopGroup childGroup() {
        return new NioEventLoopGroup(ClientConfiguration.CHILD_COUNT);
    }

    public Promise<Response> exchange(Request request) {
        if(channel.isActive()) {
            final Promise<Response> promise = channel.eventLoop().newPromise();
            queue.add(promise);
            sendJsonRequest(request);
            return promise;
        } else {
            throw new IllegalStateException("Channel is closed or was not opened yet. Try to reconnect");
        }
    }

    private void sendJsonRequest(Request request) {
        try {
            final String jsonRequest = objectMapper.writeValueAsString(request);
            logger.debug("Sending message #{}", request.getId());
            channel.writeAndFlush(jsonRequest + "\n\r");
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

}
