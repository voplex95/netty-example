package netty.server;

import io.netty.channel.ChannelInboundHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.json.JsonObjectDecoder;
import io.netty.handler.codec.string.StringEncoder;

public class SocketChannelInitializer extends ChannelInitializer<SocketChannel> {

    private static final ChannelInboundHandler inboundHandler = new SocketChannelInboundHandler();

    @Override
    protected void initChannel(SocketChannel channel) {
        final ChannelPipeline pipeline = channel.pipeline();
        pipeline.addLast(new JsonObjectDecoder());
        pipeline.addLast(new StringEncoder());
        pipeline.addLast(inboundHandler);
    }

    public static ChannelInboundHandler getInboundHandler() {
        return inboundHandler;
    }

}
