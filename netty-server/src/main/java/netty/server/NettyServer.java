package netty.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import netty.commons.ServerConfiguration;

import java.net.InetSocketAddress;

public class NettyServer {

    private Channel serverChannel;

    public void start() {
        final ServerBootstrap serverBootstrap = serverBootstrap();
        try {
            serverChannel = serverBootstrap.bind(inetSocketAddress()).sync().channel().closeFuture().sync().channel();
        } catch(InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private ServerBootstrap serverBootstrap() {
        final ServerBootstrap serverBootstrap = new ServerBootstrap();
        serverBootstrap
                .group(parentGroup(), childGroup())
                .channel(NioServerSocketChannel.class)
                .option(ChannelOption.SO_BACKLOG, ServerConfiguration.SO_BACKLOG)
                .childOption(ChannelOption.SO_KEEPALIVE, ServerConfiguration.SO_KEEPALIVE)
                .handler(new LoggingHandler(LogLevel.DEBUG))
                .childHandler(new SocketChannelInitializer());

        return serverBootstrap;
    }

    private EventLoopGroup parentGroup() {
        return new NioEventLoopGroup(ServerConfiguration.PARENT_COUNT);
    }

    private EventLoopGroup childGroup() {
        return new NioEventLoopGroup(ServerConfiguration.CHILD_COUNT);
    }

    private InetSocketAddress inetSocketAddress() {
        return new InetSocketAddress(ServerConfiguration.PORT);
    }

    public void stop() {
        serverChannel.close();
        serverChannel.parent().close();
    }

}
