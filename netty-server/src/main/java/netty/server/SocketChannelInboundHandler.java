package netty.server;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import netty.commons.Request;
import netty.commons.Response;
import netty.commons.SharedObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.Charset;

@Sharable
public class SocketChannelInboundHandler extends ChannelInboundHandlerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(SocketChannelInboundHandler.class);

    private ChannelRepository channelRepository = ChannelRepository.sharedInstance();
    private ObjectMapper objectMapper = SharedObjectMapper.getInstance();

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        ctx.fireChannelActive();
        final String channelKey = ctx.channel().remoteAddress().toString();
        channelRepository.add(channelKey, ctx.channel());
        logger.debug("Registered new active channel. Remote address is {}", channelKey);
        logger.debug("Number of active channels: {}", channelRepository.count());
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) {
        final String channelKey = ctx.channel().remoteAddress().toString();
        channelRepository.remove(channelKey);
        logger.debug("Unregistered channel for remote address {}", channelKey);
        logger.debug("Number of active channels: {}", channelRepository.count());
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws IOException {
        Response response;
        try {
            final String json = ((ByteBuf) msg).toString(Charset.forName("utf8"));
            logger.debug("Received message -> content[\n{}\n], remote address[{}]", json, ctx.channel().remoteAddress());
            logger.debug("Attempting to deserialize JSON message");

            final Request request = objectMapper.readValue(json, Request.class);
            logger.debug("{}", request);

            response = Response.ok();
        } catch(Exception e) {
            response = Response.badRequest();
        } finally {
            ((ByteBuf) msg).release();
        }
        sendResponse(ctx, response);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws JsonProcessingException {
        logger.error("Caught exception: {}", cause.getMessage(), cause);
        sendResponse(ctx, Response.internalServerError());
    }

    private void sendResponse(ChannelHandlerContext ctx, Response response) throws JsonProcessingException {
        ctx.channel().writeAndFlush(objectMapper.writeValueAsString(response) + "\n\r");
        logger.debug("Response was written into the channel");
    }
}
