package netty.server;

import io.netty.channel.Channel;

import java.util.HashMap;
import java.util.Map;

public class ChannelRepository {

    private static final ChannelRepository channelRepository = new ChannelRepository();

    public static ChannelRepository sharedInstance() {
        return channelRepository;
    }

    private final Map<String, Channel> activeChannels = new HashMap<>();

    private ChannelRepository() {}

    public Channel get(String key) {
        return activeChannels.get(key);
    }

    public void add(String key, Channel channel) {
        activeChannels.put(key, channel);
    }

    public void remove(String key) {
        activeChannels.remove(key);
    }

    public int count() {
        return activeChannels.size();
    }
}
